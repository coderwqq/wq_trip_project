import hyRequest from "../request";

export function getOrdersList(type) {
  return hyRequest.get({
    url: "order/list",
    params: {
      type,
    },
  });
}
