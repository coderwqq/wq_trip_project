import { defineStore } from "pinia";
import { getHotSuggests, getCategories, getHandpickList } from "@/services";

const useHomeStore = defineStore("home", {
  state: () => ({
    hotSuggests: [],
    categories: [],
    currentPage: 1,
    handpickList: [],
  }),
  actions: {
    async fetchHotSuggestsData() {
      const res = await getHotSuggests();
      this.hotSuggests = res.data;
    },
    async fetchCategoriesData() {
      const res = await getCategories();
      this.categories = res.data;
    },
    async fetchHandpickListData() {
      const res = await getHandpickList(this.currentPage);
      this.handpickList.push(...res.data);
      this.currentPage++;
    },
  },
});

export default useHomeStore;
