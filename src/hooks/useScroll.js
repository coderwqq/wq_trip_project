import { onMounted, onUnmounted, ref } from "vue";
import { throttle } from "underscore";

export default function useScroll(elRef) {
  let el = window;

  const isReachBottom = ref(false);

  const scrollTop = ref(0);
  const clientHeight = ref(0);
  const scrollHeight = ref(0);

  const scrollListenerHandle = throttle(() => {
    if (el === window) {
      clientHeight.value = document.documentElement.clientHeight;
      scrollTop.value = document.documentElement.scrollTop;
      scrollHeight.value = document.documentElement.scrollHeight;
    } else {
      clientHeight.value = el.clientHeight;
      scrollTop.value = el.scrollTop;
      scrollHeight.value = el.scrollHeight;
      console.log(clientHeight.value, scrollTop.value, scrollHeight.value);
    }

    if (clientHeight.value + scrollTop.value >= scrollHeight.value) {
      console.log("滚到底部了");
      // 2.方法二
      isReachBottom.value = true;

      // 1.方法一
      // if (reachBottomCB) {
      //   return reachBottomCB();
      // }
    }
  }, 100);

  onMounted(() => {
    if (elRef) {
      el = elRef.value;
    }
    el.addEventListener("scroll", scrollListenerHandle);
  });
  onUnmounted(() => {
    el.removeEventListener("scroll", scrollListenerHandle);
  });

  return { isReachBottom, scrollTop, clientHeight, scrollHeight };
}
