export const getAssetURL = (image) => {
  return new URL(`../assets/image/${image}`, import.meta.url).href
}